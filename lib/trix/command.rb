module Trix
  module Command
    From = /(.)([A-Z])/
    To   = '\1-\2'

    class << self
      def underscore_klass(klass)
        Trix::Util.demodulize(klass).gsub(From, To).downcase
      end

      def get_option_name_for(klass, with:)
        result = []

        result << with
        result << underscore_klass(klass)
        result << @option_group if @option_group

        result.join('-')
      end

      def option_for(klass, aliases: [])
        @modifiers.each do |m|
          option_procs << proc do
            option ::Trix::Command.get_option_name_for(klass, with: m), type: :boolean, default: false, aliases: Array(aliases)
          end
        end
      end

      def option_group(option, *args)
        @option_group = option
        @modifiers    = args
        yield
      ensure
        @option_group = nil
        @modifiers    = nil
      end

      def option_procs
        @option_procs ||= []
      end

      def configure_options_for(thor_klass)
        option_procs.each do |proc|
          thor_klass.instance_exec(&proc)
        end
      end
    end
  end
end
