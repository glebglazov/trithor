require 'open3'

require 'trix/system/execution_error'

module Trix
  module System
    Call = Struct.new(:stdout, :stderr, :status) do
      def output
        stdout.chomp
      end

      def error
        stderr.chomp
      end

      def success?
        status.success?
      end

      def failure?
        !success?
      end
    end

    class << self
      def stream3(*cmd)
        Open3.popen3(*cmd) do |i, o, e, t|
          output = []

          o.each do |l|
            line = l.chomp

            output << line

            yield line if block_given?
          end

          [output.join("\n"), e.read, t.value]
        end
      end

      def execute(command, throwable: true, **options, &block)
        result = Call.new(*stream3(command, &block))

        fail(ExecutionError.new(command, result.error)) if result.failure? && throwable

        result.output
      end
    end
  end
end
