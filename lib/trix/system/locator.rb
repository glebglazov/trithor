module Trix
  module System
    module Locator
      Delimeter = '/'.freeze

      class << self
        def current_path
          `pwd`.chomp
        end

        def backward_traversal_for(path)
          splitted_path = path.split(Delimeter)

          (splitted_path.count - 1).times do |t|
            top_index = -(1 + t)

            yield splitted_path[0..top_index]
          end

          nil
        end

        def app_path
          @app_path ||= begin
                          app_path = backward_traversal_for(current_path) do |path|
                            break path if path ~= /Gemfile\Z/
                          end

                          app_path || nil
                        end
        end

        def gem_path
          @gem_path ||= begin
                          System.execute("cd #{app_path} && bundle show trinity".chomp)
                        rescue System::ExecutionError
                          nil
                        end
        end
      end
    end
  end
end
