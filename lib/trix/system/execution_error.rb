module Trix
  module System
    class ExecutionError < StandardError
      def initialize(command, stderr)
        super()

        @command = command
        @stderr  = stderr
      end

      def message
        output = ["Error while executing: #{@command}"]

        output << "Stderr: #{@stderr}" unless @stderr.empty?

        output.join("\n")
      end
    end
  end
end
