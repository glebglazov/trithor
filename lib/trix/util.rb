module Trix
  module Util
    KeyDelimeter = '_'

    class << self
      def ldiff(lhs, rhs)
        lhs - lhs & rhs
      end

      def demodulize(mod)
        mod.name.split('::')[-1]
      end

      def reject_falsey(*args)
        args.flatten.select { |v| v && !v.empty? }
      end

      def build_key(*args)
        reject_falsey(*args).join(KeyDelimeter)
      end
    end
  end
end
