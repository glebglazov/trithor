require 'active_support/all'

module Trix
  module Provision
    class Options
      Defaults = {
        app_path: Trix::System::Locator.app_path,
        gem_path: Trix::System::Locator.gem_path
      }

      def self.build(options)
        return options if options.is_a?(self)

        new(options)
      end

      attr_reader :options

      delegate :[], :fetch, :values_at, to: :options

      def initialize(options)
        @options = Defaults.merge(normalize_options(options))
      end

      private

      def normalize_options(h, underscore: true)
        h.each_with_object({}) do |(k, v), memo|
          k = k.gsub(/-/, '_') if k.is_a?(String) && underscore

          memo[k.to_sym] = if v.is_a?(Hash)
                             normalize_options(v, underscore: true)
                           else
                             v.presence
                           end
        end
      end
    end
  end
end

Trix::Provision::Options.build('a' => 'b', 'c-d' => { 'd' => '' })
