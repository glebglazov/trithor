module Trix
  module Provision
    class Chain
      attr_reader :reporter

      def initialize(proxy_target, options)
        @reporter  = options.fetch(:reporter_type, Provision::Reporters::Thor).new(proxy_target)
        @options   = options
        @procs     = []
      end

      def call
        trap_signal!

        @procs.inject(make_result) do |result, proc|
          begin
            break reporter.failure(result.fetch(:reason, 'For no good reason')) if result.abort?
            next reporter.note(result.reason) if result.skip?

            instance = proc.call(reporter, result)

            next reporter.note("Skipping #{instance.class.name} step") if false

            instance.call
          end
        end
      end

      def register(klass)
        @procs.push lambda { |*args| klass.new(*args) }
      end

      private

      def trap_signal!
        Signal.trap('USR1') do
          reporter.warning 'Terminating chain...'

          exit 1
        end
      end

      def make_result
        Result.new(Result::OK, data: @options)
      end
    end
  end
end
