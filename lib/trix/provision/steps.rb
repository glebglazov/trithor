module Trix
  module Provision
    module Steps
      Modifiers = [:skip, :force]
      Registry  = Hash.new([])

      def self.register(klass, groups: [], aliases: [])
        Modifiers.each do |modifier|
          Registry[klass][modifier] = []

          (aliases + groups).each do |extra|
            Registry[klass][modifier] += ["#{modifier}_#{extra}".to_sym]
          end
        end
      end
    end
  end
end

require 'trix/provision/steps/initializers/preloader'
require 'trix/provision/steps/initializers/configuration'
require 'trix/provision/steps/initializers/git_submodule'

require 'trix/provision/steps/assembly/bundler'
require 'trix/provision/steps/assembly/database'
require 'trix/provision/steps/delivery/launch'
require 'trix/provision/steps/delivery/web_server'
require 'trix/provision/steps/fetchers/application'
require 'trix/provision/steps/fetchers/gem'
require 'trix/provision/steps/gemfile/morpher'
