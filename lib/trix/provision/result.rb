module Trix
  module Provision
    class Result
      attr_reader :status, :data

      delegate :[], :fetch, :values_at, to: :data

      Statuses = [
        OK    = :ok,
        ABORT = :abort,
        SKIP  = :skip
      ]

      def initialize(status, data: {})
        fail RuntimeError unless Statuses.include?(status)

        @status = status
        @data   = Trix::Provision::Options.build(data)
      end

      Statuses.each do |v|
        define_method(v) do |**options|
          with(self.class.const_get(v.upcase), options)
        end

        define_method("#{v}?") do
          status == self.class.const_get(v.upcase)
        end
      end

      def with(status, **options)
        self.class.new(status, data: data.merge(options))
      end
    end
  end
end
