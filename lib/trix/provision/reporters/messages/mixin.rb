module Trix
  module Provision
    module Reporters
      module Messages
        module Mixin
          def self.extended(base)
            base.include InstanceMethods
          end

          def message(type, helper: true, **options)
            if helper && !instance_methods.include?(type)
              define_method(type) do |text = ''|
                print(build_message_for(text, type: type))
              end
            end

            current_configuration = messages_configuration.fetch(type, {}).to_h
            updated_configuration = current_configuration.merge(options).merge(type: type)

            messages_configuration[type] = Builder.new(updated_configuration)
          end

          def messages_configuration
            @@messages_configuration ||= {}
          end

          private

          module InstanceMethods
            def build_message_for(text, type: Basic)
              builder = self.class.send(:messages_configuration)[type]

              (builder && builder.call(text)) || fail(RuntimeError)
            end
          end
        end
      end
    end
  end
end
