module Trix
  module Provision
    module Reporters
      module Messages
        class Builder
          Delimeter = ' '

          attr_reader :type, :prefix, :postfix, :titleize, :newline

          def initialize(type:, prefix: nil, postfix: nil, titleize: true, newline: true)
            @type     = type.to_s
            @prefix   = prefix
            @postfix  = postfix
            @titleize = titleize
            @newline  = newline
          end

          def call(text)
            body   = []
            result = []

            body << title if titleize
            body << text

            result << prefix if prefix
            result << body.join(Delimeter)
            result << postfix if postfix

            return "\n" + result.join if newline

            result.join
          end

          def to_h
            {
              type: type,
              prefix: prefix,
              postfix: postfix,
              titleize: titleize,
              newline: newline
            }
          end

          private

          def title
            @title ||= type.downcase.gsub(/^./, type[0].upcase) + ':'
          end
        end
      end
    end
  end
end
