module Trix
  module Provision
    module Reporters
      class Base
        extend Messages::Mixin

        AskAgreementRegexp = /(y|yes|^)$/i

        List = [
          Basic    = :basic,
          Question = :question,
          Finish   = :finish,
          Note     = :note,
          Start    = :start,
          Failure  = :failure,
          Warning  = :warning
        ]

        message Question, titleize: false, newline: false, helper: false, postfix: ' (y/n)'
        message Basic, titleize: false, newline: false
        message Finish, newline: false
        message Note
        message Start
        message Failure
        message Warning

        attr_reader :proxy_target

        def initialize(proxy_target)
          @proxy_target = proxy_target
        end

        def print(text, type: Basic, **options)
          __print__(build_message_for(text, type: type, **options), **options)
        end

        def ask(text, **options)
          __ask__(build_message_for(text, type: Question, **options)) =~ AskAgreementRegexp
        end

        def wrap(text, **options)
          print(text, type: Start, **options)

          with_benchmark_for(text) { yield }
        end

        private

        def with_benchmark_for(text, **options)
          s = Time.now
          yield
          f = (Time.now - s).floor

          result = f > 0 ? "#{f} second(s)" : 'no time'

          print("#{text} in #{result}", type: Finish, **options)
        end

        def __ask__(question, **options); fail NotImplementedError end
        def __print__(message, **options); fail NotImplementedError end
      end
    end
  end
end
