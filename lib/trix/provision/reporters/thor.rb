module Trix
  module Provision
    module Reporters
      class Thor < Base
        EmptyMessage = ''.freeze

        Color = ::Thor::Shell::Color
        ColorsMap = {
          Basic => Color::WHITE,
          Start => Color::GREEN,
          Finish => Color::GREEN,
          Note => Color::BLUE,
          Warning => Color::YELLOW,
          Failure => Color::RED,
          Question => Color::CYAN
        }

        ColorsMap.each do |k, v|
          message k, prefix: v
        end

        def wrap(message, **options, &block)
          result = nil

          super(message, **options) do
            result = block.call
          end

          result
        end

        private

        def __print__(message, **options)
          proxy_target.say(message)
          proxy_target.set_color(Color::WHITE)
        end

        def __ask__(message, **options)
          proxy_target.ask(message)
        end
      end
    end
  end
end
