require 'trix/provision/steps/assembly/base'

module Trix
  module Provision
    module Steps
      module Assembly
        class Database < Base
          StaleMessageProc = proc { |stale|
<<EOS
Your app state is probably not stale.
Assumptions:
#{stale.join("\n")}
Do you want to drop, create and migrate database?
EOS
  .chomp
          }

          define_option_method(:refresh_db)

          def call
            refresh? ? db_refresh : db_migrate

            result.ok
          end

          private

          def skip?
            super || (stale? && !force && !refresh_db?)
          end

          def stale?
            !brand_new_assumptions?
          end

          def refresh?
            @refresh ||= refresh_db? || (!stale? && ensure_stale_prediction_is_right?)
          end

          def ensure_stale_prediction_is_right?
            ask(StaleMessageProc.call(brand_new_assumptions))
          end

          def db_refresh
            db_drop
            db_create
            db_migrate
            db_seed
          end

          def db_drop
            wrap 'Dropping database' do
              execute_within_app_root('bundle exec rake db:drop')
            end
          end

          def db_create
            wrap 'Creating database' do
              execute_within_app_root('bundle exec rake db:create')
            end
          end

          def db_migrate
            wrap 'Migrating database' do
              execute_within_app_root('bundle exec rake db:migrate')
            end
          end

          def db_seed
            wrap 'Seeding database' do
              execute_within_app_root('bundle exec rake trinity:seed')
            end
          end
        end
      end

      register Assembly::Database, groups: [:assembly]
    end
  end
end
