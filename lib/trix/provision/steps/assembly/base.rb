module Trix
  module Provision
    module Steps
      module Assembly
        class Base < Steps::Base
          define_option_method(:brand_new_assumptions) { [] }
        end
      end
    end
  end
end
