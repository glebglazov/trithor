require 'trix/provision/steps/assembly/base'

module Trix
  module Provision
    module Steps
      module Assembly
        class Bundler < Base
          def call
            wrap 'Running bundle install' do
              execute_within_app_root('bundle install')
            end

            result.ok
          rescue Trix::System::ExecutionError
            failure 'bundle install'

            wrap 'Running bundle update instead' do
              execute_within_app_root('bundle update')
            end

            result.ok
          end
        end
      end

      register Assemble::Bundler, groups: [:assembly]
    end
  end
end
