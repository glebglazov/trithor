require 'trix/provision/steps/delivery/base'

module Trix
  module Provision
    module Steps
      module Delivery
        class Launch < Base
          DefaultOptions = %i(application environment format tunneling server)

          attr_reader *DefaultOptions.map(&:to_sym)

          def initialize(*)
            super

            @file_or_dir =
              if path = result[:path]
                File.join(features_path, path)
              else
                features_path
              end

            @application, @environment, @format, @tunneling, @server = result.values_at(*DefaultOptions)
          end

          def call
            wrap 'Spec launch' do
              touch_rspec_log_folder!

              spawn!

              while true
                result = begin
                           Process.kill(0, self.class.pid)

                           true
                         rescue Errno::EPERM
                           false
                         end

                break unless result

                sleep 1
              end
            end

            result.ok
          end

          private

          def spawn_command
            "cd #{app_path} && " + (env_vars + rspec_command + redirect_pipe).join(' ')
          end

          def env_vars
            result = {}

            result['APP']             = application
            result['ENV']             = tunneling ? Options::Environments::Trix : environment
            result['RAILS_ENV']       = environment
            result['DISABLE_SPRING']  = ENV['DISABLE_SPRING']
            result['BUNNY_LOG_LEVEL'] = "INFO"
            result['LOCALHOST']       = true if tunneling

            result.map { |v| v.join('=') }
          end

          def rspec_command
            result = []

            result << 'rspec'
            result << @file_or_dir

            result << '--tty'
            result << "--require #{spec_helper_path}"

            if format && (format_options = format_map[format])
              result << "--require #{format_options[:require]}"
              result << "--format #{format_options[:format]}"
            else
              result << '--format documentation --color'
            end

            result
          end

          def touch_rspec_log_folder!
            `mkdir -p #{rspec_log_folder_path}`
          end

          def rspec_log_folder_path
            @rspec_log_path ||= File.expand_path('./log/rspec', app_path)
          end

          def redirect_pipe
            Array("2>&1 | tee -a #{rspec_log_folder_path}/#{Time.now.to_i}.txt")
          end

          def root_specs_path
            @root_specs_path ||= File.expand_path('./spec', gem_path)
          end

          def features_path
            @features_path ||= File.expand_path('./features', root_specs_path)
          end

          def spec_helper_path
            @spec_helper_path ||= File.expand_path('./trinity_spec_helper.rb', root_specs_path)
          end

          def format_map
            @format_map ||=  {
              'json' => {
                format:  'Trinity::Formatters::JSON',
                require: File.expand_path('./lib/trinity/formatters/json.rb', gem_path)
              }
            }
          end
        end
      end

      register Delivery::Launch, groups: [:delivery]
    end
  end
end
