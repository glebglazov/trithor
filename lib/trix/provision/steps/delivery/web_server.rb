require 'thin'

require 'trix/provision/steps/delivery/base'

module Trix
  module Provision
    module Steps
      module Delivery
        class WebServer < Base
          def call
            touch_web_server_log_folder!

            wrap 'Web-server start' do
              spawn!

              sleep 15
            end

            result.ok
          end

          private

          def spawn_command
            "cd #{app_path} && bundle exec thin start --port 3500 #{redirect_to_web_server_log}"
          end

          def skip?
            super || environment != 'development'
          end

          def touch_web_server_log_folder!
            `mkdir -p #{web_server_log_folder_path}`
          end

          def web_server_log_folder_path
            @web_server_log_folder_path ||= File.expand_path('./log/web_server')
          end

          def web_server_log_path
            @web_server_log_path ||= File.expand_path("./ws-#{Time.now.to_i}.log", web_server_log_folder_path)
          end

          def redirect_to_web_server_log
            "> #{web_server_log_path}"
          end
        end
      end

      register Delivery::WebServer, groups: [:delivery]
    end
  end
end
