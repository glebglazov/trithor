module Trix
  module Provision
    module Steps
      module Delivery
        class Base < Steps::Base
          class << self
            attr_accessor :pid
          end

          def spawn!
            self.class.pid ||= Process.spawn(spawn_command, in: '/dev/null')
          end

          private

          def spawn_command
            fail NotImplementedError
          end
        end
      end
    end
  end
end
