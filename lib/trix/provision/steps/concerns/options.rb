module Trix
  module Provision
    module Steps
      module Concerns
        module Options
          Common = [:skip, :force]

          def self.included(base)
            base.send(:extend, ClassMethods)

            Common.each do |option_name|
              base.define_option_method(option_name) do
                fetch(Trix::Command.get_option_name_for(self.class, with: option_name).gsub(/-/, '_'), false)
              end
            end
          end

          module ClassMethods
            def define_option_method(option_name, method: {}, key: {}, **options, &default)
              method_name          = Util.build_key([method[:prefix], option_name, method[:affix]])
              key_name             = Util.build_key([key[:prefix], option_name, key[:affix]])

              ivar_name            = "@#{key_name}"
              presence_method_name = "#{method_name}?"

              define_method(method_name) do
                value = instance_variable_get(ivar_name)
                return value if value

                value = fetch(key_name, false)
                return instance_variable_set(ivar_name, value) if value

                value = if default
                          instance_exec(&default)
                        else
                          ancestor = self.class.ancestors[1]

                          super() if ancestor && ancestor.respond_to?(method_name)
                        end

                instance_variable_set(ivar_name, value)
              end

              define_method(presence_method_name) do
                result = public_send(method_name)

                return !result.empty? if result.respond_to?(:empty?)

                !!result
              end
            end
          end
        end
      end
    end
  end
end
