module Trix
  module Provision
    module Steps
      module Concerns
        module Scoping
          All  = :all
          List = [:app, :gem]

          AllowedScopes = [All, *List]

          def self.included(base)
            base.send(:extend, ClassMethods)
          end

          List.each do |scope|
            method_name = "execute_within_#{scope}_root"

            define_method(method_name) do |command, **options|
              path = send("#{scope}_path")

              execute("cd #{path} && #{command}", **options)
            end
          end

          module ClassMethods
            def define_scoped_option_method(option_name, scope: :all, **options)
              scope = List if scope == All
              scopes = Array(scope)

              disallowed = Trix::Util.ldiff(scopes, List)

              fail RuntimeError, "You have passed disallowed scope(s): #{disallowed}" unless disallowed.empty?

              define_option_method(option_name, key: { prefix: scope }, **options) if scopes.length == 1

              scopes.each do |scope|
                define_option_method(option_name, method: { prefix: scope }, key: { prefix: scope }, **options)
              end
            end
          end
        end
      end
    end
  end
end
