require 'trix/provision/steps/fetchers/strategies/base'

module Trix
  module Provision
    module Steps
      module Fetchers
        class Gem < Base
          EdgeMapping = {
            'hov' => 'heart_of_vegas',
            'cms' => 'cashman'
          }

          def self.scope
            :gem
          end

          define_scoped_option_methods

          def branch
            return EdgeMapping[application] if edge?

            @branch || 'HEAD'
          end
        end
      end

      register Fetchers::Gem, groups: [:fetch]
    end
  end
end
