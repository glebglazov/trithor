require 'delegate'

require 'trix/provision/steps/fetchers/strategies/local'
require 'trix/provision/steps/fetchers/strategies/remote'

module Trix
  module Provision
    module Steps
      module Fetchers
        class Base < Steps::Base
          EdgeRemote    = 'origin'.freeze
          EdgeBranch    = 'master'.freeze
          ScopedOptions = %i[path remote branch stash edge]

          class << self
            def scope
              fail NotImplementedError
            end

            def strategies
              @strategies ||= [Strategies::Local, Strategies::Remote]
            end

            def define_scoped_option_methods
              ScopedOptions.each do |option_name|
                define_scoped_option_method(option_name, scope: scope)
              end
            end
          end

          def call
            upgrade_codebase

            result.ok
          end

          def execute_within_scope_root(command, **options)
            send("execute_within_#{self.class.scope}_root", command, **options)
          end

          def head?
            branch == 'HEAD'
          end

          private

          def upgrade_codebase
            strategy.call
          end

          def strategy
            self.class.strategies.map { |s| s.new(self) }.detect { |s| s.satisfy? }
          end
        end
      end
    end
  end
end
