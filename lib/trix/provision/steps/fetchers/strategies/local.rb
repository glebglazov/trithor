require 'trix/provision/steps/fetchers/strategies/base'

module Trix
  module Provision
    module Steps
      module Fetchers
        module Strategies
          Checks = [
            [-> (o) { o.edge? }, 'edge is present'],
            [-> (o) { o.remote? }, 'remote is present'],
            [-> (o) { !o.requested_branch? }, 'requested branch is absent']
          ]

          class Local < Base
            def call
              return notify_check_skip('branch is set to HEAD') if head?

              super
            end

            def satisfy?
              Checks.each do |check, reason|
                if check.call(self)
                  notify_check_skip(reason)

                  return false
                end
              end

              true
            end

            private

            def notify_check_skip(reason)
              output = []

              output << "Skipping #{self.class.humanized_name}"
              output << "cause #{reason}" if reason

              note(output.join(' '))
            end

            def checkout
              "git checkout #{branch}"
            end
          end
        end
      end
    end
  end
end
