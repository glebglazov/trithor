module Trix
  module Provision
    module Steps
      module Fetchers
        module Strategies
          class Base < SimpleDelegator
            def call
              note("Using fetch via #{self.class.humanized_name} strategy for #{__getobj__.class.name}")

              commands_list.each do |command|
                execute_within_scope_root(command)
              end
            end

            def satisfy?
              fail NotImplementedError
            end

            def requested_branch?
              @_requested_branch ||= head? || execute_within_scope_root("git branch | grep #{branch}")
            end

            private

            def commands_list
              @commands_list ||= Util.reject_falsey([fetch, checkout, stash, reset].flatten)
            end

            def checkout; end

            def fetch; end

            def stash
              'git stash' if stash?
            end

            def reset; end

            def self.humanized_name
              Trix::Util.demodulize(self).downcase
            end
          end
        end
      end
    end
  end
end
