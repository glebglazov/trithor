require 'trix/provision/steps/fetchers/strategies/base'

module Trix
  module Provision
    module Steps
      module Fetchers
        module Strategies
          class Remote < Base
            Remote = 'origin'
            Branch = 'master'

            def satisfy?
              true
            end

            private

            def resolved_remote
              @resolved_remote ||= remote? ? remote : Remote
            end

            def resolved_branch
              @resolved_branch ||= (branch? && !head?) ? branch : Branch
            end

            def checkout
              "git checkout -B #{resolved_branch}"
            end

            def fetch
              "git fetch #{resolved_remote}"
            end

            def reset
              "git reset --hard #{resolved_remote}/#{resolved_branch}"
            end
          end
        end
      end
    end
  end
end
