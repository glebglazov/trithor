require 'trix/provision/steps/fetchers/strategies/base'

module Trix
  module Provision
    module Steps
      module Fetchers
        class Application < Base
          def self.scope
            :app
          end

          def upgrade_codebase
            super

            execute_within_scope_root('./script/fetch_gems.rb --stash')
          end

          define_scoped_option_methods
        end
      end

      register Fetchers::Application, groups: [:fetch]
    end
  end
end
