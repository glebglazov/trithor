require 'delegate'

module Trix
  module Provision
    module Steps
      class Base < SimpleDelegator
        include Concerns::Scoping
        include Concerns::Options

        define_scoped_option_method :path, scopes: [:app, :gem]
        define_option_method :environment
        define_option_method :application

        attr_reader :reporter, :result

        def self.call(*args)
          new(*args).send(:__call__)
        end

        def initialize(reporter, result)
          super(reporter)

          @reporter = reporter
          @result   = result
        end

        def call
          fail NotImplementedError, "You should implement method call for #{self.class.name}"
        end

        private

        def __call__
          return result.skip(reason: "Skipping #{self.class.name} step") if !force? && skip?

          call
        rescue StandardError => e
          begin
            return result.abort(reason: [e.message, *e.backtrace].join("\n"))
          rescue StandardError => e
            puts [e.message, *e.backtrace].join("\n")
          end
        end

        def fetch(*args, &block)
          result.fetch(*args, &block)
        end

        def execute(command, **options)
          Trix::System.execute(command, **options) { |line| reporter.print(line) }
        end
      end
    end
  end
end
