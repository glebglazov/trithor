require 'trix/provision/steps/gemfile/rule'

module Trix
  module Provision
    module Steps
      module Gemfile
        class Morpher < Steps::Base
          Message = 'Gemfile is already populated with Trinity dependencies, skipping that step'.freeze

          def call
            wrap 'Gemfile check and population' do
              lines =
                begin
                  File.readlines(gemfile_path)
                rescue IOError => e
                  return result.abort(reason: e.message)
                end

              temp = []

              lines.each do |line|
                next if pry_rule.check?(line) || trinity_rule.check?(line)

                temp.push line
              end

              temp.push pry_rule.exact_line
              temp.push trinity_rule.exact_line

              File.open(gemfile_path, 'w+') { |f| f.puts(temp) }
            end

            result.ok
          end

          private

          def pry_rule
            @pry_rule ||= Rule.new('pry-byebug', type: :github, locator: 'deivid-rodriguez/pry-byebug', ref: '5c72fb')
          end

          def trinity_rule
            @trinity_rule ||= Rule.new('trinity', type: :path, locator: gem_path)
          end

          def gemfile_path
            @gemfile_path ||= File.expand_path('./Gemfile', app_path)
          end
        end
      end

      register Gemfile::Morpher
    end
  end
end
