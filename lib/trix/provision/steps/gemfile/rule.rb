module Trix
  module Provision
    module Steps
      module Gemfile
        class Rule
          attr_reader :gem_name, :type, :locator, :ref

          def initialize(gem_name, type: :path, locator: nil, ref: nil)
            @gem_name = gem_name
            @type     = type
            @locator  = locator
            @ref      = ref
          end

          def check?(line)
            line =~ gem_base_regexp
          end

          def exact_line
            @exact_line ||= [gem_base, location, revision].reject(&:nil?).join(', ')
          end

          private

          def gem_base_regexp
            @gem_base_regexp ||= /#{gem_base}/
          end

          def gem_base
            @gem_base ||= "gem '#{gem_name}'"
          end

          def location
            @location ||= "#{type}: '#{locator}'" if type && locator
          end

          def revision
            @revision ||= "ref: '#{ref}'" if ref
          end
        end
      end
    end
  end
end
