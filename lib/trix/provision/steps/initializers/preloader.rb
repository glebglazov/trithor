module Trix
  module Provision
    module Steps
      module Initializers
        class Preloader < Steps::Base
          NoGemPathMessage = <<-EOS
  You should specify Trinity gem path either:
  1) via --gem option;
  2) via application Gemfile;
  3) via TRINITY_GEM_PATH env variable.
  EOS
  .chomp

          VerificationQuestionLambda = lambda do |app_path, gem_path|
  <<-EOS
  Application path is: #{app_path}.
  Trinity path is: #{gem_path}.
  Do you wish to proceed?
  EOS
  .chomp
          end

          def call
            return result.abort(reason: NoGemPathMessage)           unless gem_found?
            return result.abort(reason: 'Finishing Trinity script') unless do_run?

            result.ok(app_path: resolved_app_path, gem_path: resolved_gem_path)
          end

          private

          def gem_found?
            !!resolved_gem_path
          end

          def do_run?
            reporter.ask(VerificationQuestionLambda.call(resolved_app_path, resolved_gem_path))
          end

          def skip?
            false
          end

          def expand_home_dir(path)
            path && path.gsub(/\A~/, Dir.home)
          end

          %w[app gem].each do |v|
            original_method_key = "#{v}_path"

            resolve_method_key  = "resolve_#{original_method_key}"
            resolved_method_key = "resolved_#{original_method_key}"

            define_method(resolve_method_key) do
              path = send(original_method_key)
              return path unless path.empty?

              path = Trix::System::Locator.public_send(original_method_key)
              return path if path

              app_upcase   = application.upcase
              scope_upcase = v.upcase
              env_var_base = "TRINITY_#{scope_upcase}_PATH"

              ENV["#{app_upcase}_#{env_var_base}"] || ENV[env_var_base]
            end

            define_method(resolved_method_key) do
              result = instance_variable_get("@#{resolved_method_key}")

              return result if result

              instance_variable_set("@#{resolved_method_key}", expand_home_dir(send(resolve_method_key)))
            end
          end
        end
      end
    end
  end
end
