require 'trix/provision/steps/initializers/base'

module Trix
  module Provision
    module Steps
      module Initializers
        class GitSubmodule < Base
          private

          def operate
            wrap "Git submodule population" do
              git_submodule_update
              script_fetch_gems_all
            end
          end

          def brand_new_assumption
            @brand_new_assumption ||= "Git submodule was not yet checked out"
          end

          def git_submodule_init
            execute_within_app_root('git submodule init')
          end

          def git_submodule_update
            execute_within_app_root('git submodule update')
          end

          def script_fetch_gems_all
            execute_within_app_root('./script/fetch_gems.rb --all')
          end

          def stale?
            @stale ||= git_submodule_init.empty?
          end
        end
      end
    end
  end
end
