require 'trix/provision/steps/initializers/base'

module Trix
  module Provision
    module Steps
      module Initializers
        class Configuration < Base
          private

          def operate
            wrap "Configuration copying" do
              copy_config_files
            end
          end

          def brand_new_assumption
            @brand_new_assumption ||= "Some configuration files are missing at #{app_path}/config: \n#{missing_files}"
          end

          def copy_config_files
            system("cp #{config_snippets_path}/* #{config_path}")
          end

          def config_path
            File.expand_path("./config", app_path)
          end

          def config_snippets_path
            File.expand_path("./configs", config_path)
          end

          def config_list
            @config_dir_list ||= files_list_for(config_path)
          end

          def config_snippets_list
            @configs_list ||= files_list_for(config_snippets_path)
          end

          def missing_files
            @missing_files ||= Trix::Util.ldiff(config_snippets_list, config_list)
          end

          def files_list_for(path)
            Dir.entries(path)
          end

          def stale?
            @stale ||= missing_files.empty?
          end
        end
      end
    end
  end
end
