module Trix
  module Provision
    module Steps
      module Initializers
        class Base < Steps::Base
          def call
            operate

            result.ok(brand_new_assumptions: brand_new_info)
          end

          private

          def operate
            fail NotImplementedError
          end

          def brand_new_assumption
            fail NotImplementedError
          end

          def brand_new_info
            s = fetch(:brand_new, [])
            s += Array(brand_new_assumption) if brand_new?
            s
          end

          def brand_new?
            !stale?
          end

          def stale?
            false
          end

          def skip?
            stale?
          end
        end
      end
    end
  end
end
