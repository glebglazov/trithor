module Trix
  module Provision
    class DSL
      def self.call(thor, options = {}, &block)
        new(thor, options, &block).call
      end

      def initialize(thor, options, &block)
        @thor  = thor || fail(RuntimeError, 'Thor instance is required')
        @chain = Chain.new(thor, options)
        @block = block
      end

      def call
        step(Steps::Initializers::Preloader)
        step(Steps::Initializers::Configuration)
        step(Steps::Initializers::GitSubmodule)

        instance_exec(&@block)

        @chain
      end

      private

      def step(klass)
        @chain.register klass
      end
    end
  end
end
