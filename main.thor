$:.unshift(File.expand_path('./lib', File.dirname(__FILE__)))

require 'thor'
require 'open-uri'
require 'pry'

require 'trix'

module ::Trix
  S = Provision::Steps

  class Main < ::Thor
    namespace :trinity

    method_option :gem_path, type: :string, default: ''
    method_option :gem_remote, type: :string, default: ''
    method_option :gem_branch, type: :string, default: 'HEAD'
    method_option :gem_stash, type: :boolean, default: false
    method_option :gem_edge, type: :boolean, default: false

    method_option :app_path, type: :string, default: ''
    method_option :app_remote, type: :string, default: ''
    method_option :app_branch, type: :string, default: 'HEAD'
    method_option :app_stash, type: :boolean, default: true
    method_option :app_edge, type: :boolean, default: false

    method_option :application, type: :string, aliases: :a, default: 'hov', enum: ['hov', 'cms']
    method_option :environment, type: :string, aliases: :e, default: 'development'
    method_option :tunneling, type: :boolean, aliases: :t, default: false
    method_option :format, type: :string, aliases: :f, default: 'none', enum: ['none', 'json']
    method_option :server, type: :string, aliases: :s, default: ''

    option :'refresh-db', type: :boolean, aliases: :rdb, default: false

    desc 'start <&optional file_or_directory>', 'Executes spec or specs with proper preparation'
    def start(file = nil)
      chain = Provision::DSL.call(self, options.merge(path: file)) do
        step S::Fetchers::Gem
        step S::Fetchers::Application
        step S::Gemfile::Morpher
        step S::Assembly::Bundler
        step S::Assembly::Database
        step S::Delivery::WebServer
        step S::Delivery::Launch
      end

      chain.call
    end
  end
end
